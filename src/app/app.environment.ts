import { Injectable } from '@angular/core';
import { environment } from '../../src/environments/environment';

@Injectable({
    providedIn: 'root'
  })
export class env{

    public  nenv(){
        return (environment.production) ? environment.apiUrl : '';
    }
}