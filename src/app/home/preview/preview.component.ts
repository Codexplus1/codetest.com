import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { AppService } from '../../common/services/app.service';
import { List } from 'src/app/model/flickr.model';


@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.scss']
})
export class PreviewComponent implements OnInit {

  public mySubscription: Subscription;
  public selectedList:any;

  constructor(private appService:AppService) { }

  ngOnInit() {

      /**
       * event from list component via app service
       */
      this.mySubscription = this.appService.getObs.subscribe((data) => {
          
          this.selectedList = data;
      });
  }

}
