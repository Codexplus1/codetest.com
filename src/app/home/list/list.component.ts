import { Component, OnInit } from '@angular/core';
import { AppService } from '../../common/services/app.service';
import { ConstantService } from '../../common/constants/constants.service'; 
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, Subscription } from 'rxjs';

import { List } from './../../model/flickr.model';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  public path:string;
  public lists = new Array<List>();
  public mySubscription: Subscription;

  public loading:boolean = false;

  public defaultParam:string = 'cat';
  public page:number = 1;

  constructor(private appService:AppService, private constantsService: ConstantService, public http: HttpClient) { 

    this.path = this.constantsService.getPath();


    /**
     * make default request with param 'cat'
     * time out 3s
     */
    var $this = this;
    setTimeout(function(){$this.getPhotos()}, 3000);

  }

  ngOnInit() {

      /**
       * set subscription for search
       */
      this.mySubscription = this.appService.getObs.subscribe((data) => {    
        
        if(data['target'] == 'search'){

            this.defaultParam = data['param'];
            this.search(this.defaultParam);
        }

    });
  }

  /**
   * Load more image when button is clicked
   * and set pagination accordingly
   */
  loadMoreImage(){

    this.loading = true;
      
      let page = this.page += 1;
      let param = this.defaultParam + '&page='+page;
      let res = this.appService.get(this.path + '&text=' + param).subscribe(res =>{

        this.processResponse(res);
        this.loading = false;
      },res=>{});
  }

  /**
   * 
   * @param param "this is the value enter into search form"
   */
  search(param:any){

      this.loading = true;
      this.lists = new Array<List>();
      let res = this.appService.get(this.path + '&text=' + param).subscribe(res =>{

        this.processResponse(res);
        this.loading = false;
      },res=>{});
  }

  /**
   * 
   * @param list of selected image pulled from 
   * List model
   */
  getSelected(list:any){
      this.appService.publish(list);
  }

  /**
   * inital request to api
   */
  getPhotos(){

    this.loading = true;
      let res = this.appService.get(this.path + '&text=' + this.defaultParam ).subscribe(res =>{

        this.processResponse(res);
        this.loading = false;
      },res=>{
        this.loading = false;
      });
  }

  /**
   * 
   * @param result from api call
   * 
   */
  processResponse(result){

    var _s = result.photos.photo;

    for(var z = 0 ; z < result.photos.photo.length ; z++)
    {
        var url = 'https://farm'+_s[z]['farm']+'.staticflickr.com/'+_s[z]['server']+'/'+_s[z]['id']+'_'+_s[z]['secret']+'.jpg'
        
        let row:List = new List();
        row.id = _s[z]['id'];
        row.title = _s[z]['title'];
        row.owner = _s[z]['owner'];
        row.url = url;

        this.lists.push(row);
    }

  }


}



