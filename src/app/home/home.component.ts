import { Component, OnInit } from '@angular/core';
import { AppService } from '../common/services/app.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  

  constructor(private appService:AppService) { }

  ngOnInit() {

  }

  /**
   * 
   * @param $event 
   * @param search value entered into the search form
   */
  searchImage($event:Event, search:any ){

    let params = {'target':'search', "param":search.value};
    this.appService.publish(params);
  }

}


