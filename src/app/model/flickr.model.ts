
/**
 * model used for flickr image list
 */
export class List{

    public id:number;
    public title:string;
    public url:string;
    public owner:string;
}