import { Observable, Subject } from 'rxjs';
import { throwError as observableThrowError } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { env } from '../../app.environment';
import { catchError, retry} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  private readonly subjectSource$ = new Subject<object>();
  constructor(public http: HttpClient, public env: env) { }

  /**
   * 
   * @param fn 
   * @param retryNumb 
   */
  public httpRequestStrategy(fn, retryNumb = 0){
    return fn.pipe(
      retry(retryNumb), 
      catchError(this.handleError.bind(this))
    )
  }

  /**
   * 
   * @param error 
   */
  private handleError(error:HttpErrorResponse){

    let toastMessage = 'Unable to complete request';
    if(error && error.error && error.error.message){
        toastMessage = error.error.message;
    }

    return observableThrowError(error || []);
  }

  /**
   * 
   * @param url  request path "check constants.service.ts "
   * @param options 
   */
  get(url, options = {}){

    const func = this.http.get(url, options);
    return this.httpRequestStrategy(func);
  }

  /**
   * provide access to observable
   */
  public get getObs(): Observable<object> {
      return this.subjectSource$.asObservable();
  }

  /**
   * 
   * @param data 
   */
  public publish(data: any) {
      this.subjectSource$.next(data);
  }
 


}
