import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConstantService {

  constructor() { }

  readonly API_URL = 'https://api.flickr.com/services/rest/?';

  readonly REQUEST_METHOD = 'method=flickr.photos.search';
  
  readonly APP_FILTER = '&api_key=51134a027e86dc9abca419f7ade79b87&sort=relevance&privacy_filter=1&safe_search=1&format=json&nojsoncallback=1&per_page=30';

  readonly API_SECRET_KEY = '76d1bf72991d097d';

  getPath(){
      return this.API_URL + this.REQUEST_METHOD + this.APP_FILTER;
  }

}
